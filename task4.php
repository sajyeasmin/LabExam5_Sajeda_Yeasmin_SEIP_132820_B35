<?php
class Factorial
{
    protected $_number;
    public function __construct($number)
    {
        if (!is_int($number))
        {
            throw new InvalidArgumentException('Not a number or missing argument');
        }
        $this->_number = $number;
    }
    public function result()
    {
        $x=1;
        $i=$this->_number;
        for($i;$i>=1;$i--)
        {
            $x=$x*$i;
        }
        return $x;
    }
}
$newfactorial = New Factorial(5);
echo "Factorial of 5 = ".$newfactorial->result();